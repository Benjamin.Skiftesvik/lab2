package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridgeitems = new ArrayList<FridgeItem>();
    int maxSize = 20;
    int counter;

    @Override
    public int nItemsInFridge() {
        return counter;
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge()!=maxSize){
            counter++;
            fridgeitems.add(item);
            return true;
        }
        return false;  
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge()!=0){
            fridgeitems.remove(item);
            counter--;
        }
        else{
           throw new NoSuchElementException();
        }  
        
    }

    @Override
    public void emptyFridge() {
        counter = 0;
        fridgeitems.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
    
        for (FridgeItem item: fridgeitems){
              if (item.hasExpired()){
                  expiredItems.add(item);
                  counter--;  
            }
               
        }
        return expiredItems;

    }
    
}
